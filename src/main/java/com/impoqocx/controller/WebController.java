package com.impoqocx.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author djovanovic
 */
@RestController
public class WebController {

	@GetMapping("/protected-resource")
	public String getProtectedResource() {
		return "You have successfully accessed protected resource.";
	}

}
