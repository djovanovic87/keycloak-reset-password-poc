package com.impoqocx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootKeycloakResetPasswordPOCApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKeycloakResetPasswordPOCApplication.class, args);
	}

}
