package com.impoqocx;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.HttpStatus;
import org.springframework.util.ResourceUtils;

import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author djovanovic
 */
public class KeycloakInit {

	private static Properties config = null;

	public static void main(String[] args) throws IOException {
		config = loadConfig();
		System.out.println("############################### Start Keycloak initialization ###############################");

		// Create Keycloak and get access token for admin user
		Keycloak keycloak = createKeycloak(null);

		// Create configured realm
		createRealm(keycloak);

		// Create keycloak for configured realm
		keycloak = createKeycloak(keycloak.tokenManager().getAccessTokenString());

		// Create user
		createUser(keycloak);

		// Create client
		createClient(keycloak);

		System.out.println("############################### Finished Keycloak initialization ###############################");
	}

	private static Keycloak createKeycloak(String accessToken) {
		if(accessToken == null) {
			return KeycloakBuilder.builder()
					.serverUrl(config.getProperty(KeycloakConst.KEYCLOAK_SERVER_URL_KEY))
					.realm(KeycloakConst.MASTER_REALM)
					.grantType(OAuth2Constants.PASSWORD)
					.username("admin")
					.password("admin")
					.clientId("admin-cli")
					.build();
		}

		return KeycloakBuilder.builder() //
				.serverUrl(config.getProperty(KeycloakConst.KEYCLOAK_SERVER_URL_KEY))
				.realm(config.getProperty(KeycloakConst.KEYCLOAK_REALM_KEY))
				.authorization(accessToken)//
				.build();
	}

	private static void createRealm(Keycloak keycloak) {
		String realmName = config.getProperty(KeycloakConst.KEYCLOAK_REALM_KEY);
		System.out.println("Trying to create realm");

		RealmRepresentation realmRepresentation = new RealmRepresentation();
		realmRepresentation.setRealm(realmName);
		realmRepresentation.setEnabled(true);
		realmRepresentation.setResetPasswordAllowed(true);

		Map<String, String> smtpServer = new HashMap<>();

		smtpServer.put("host", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_HOST_KEY));
		smtpServer.put("port", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_PORT_KEY));
		smtpServer.put("from", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_FROM_KEY));
		smtpServer.put("fromDisplayName", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_FROM_DISPLAY_NAME_KEY));
		smtpServer.put("auth", "true");
		smtpServer.put("user", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_USERNAME_KEY));
		smtpServer.put("password", config.getProperty(KeycloakConst.KEYCLOAK_SMTP_PASSWORD_KEY));

		realmRepresentation.setSmtpServer(smtpServer);

		keycloak
				.realms()
				.create(realmRepresentation);

		System.out.println("Successfully created realm");
	}

	private static void createUser(Keycloak keycloak) {
		System.out.println("Creating user");
		UserRepresentation userRepresentation = new UserRepresentation();

		userRepresentation.setUsername(config.getProperty(KeycloakConst.KEYCLOAK_USER_USERNAME_KEY));
		userRepresentation.setFirstName(config.getProperty(KeycloakConst.KEYCLOAK_REALM_KEY));
		userRepresentation.setEmail(config.getProperty(KeycloakConst.KEYCLOAK_USER_EMAIL_KEY));
		userRepresentation.setEnabled(true);

		CredentialRepresentation password = new CredentialRepresentation();
		password.setTemporary(false);
		password.setType(CredentialRepresentation.PASSWORD);
		String userPassword = config.getProperty(KeycloakConst.KEYCLOAK_USER_PASSWORD_KEY);
		password.setValue(userPassword);
		userRepresentation.setCredentials(Arrays.asList(password));

		Response response = keycloak.realm(config.getProperty(KeycloakConst.KEYCLOAK_REALM_KEY))
				.users()
				.create(userRepresentation);

		if(response.getStatus() != HttpStatus.CREATED.value()) {
			throw new IllegalStateException("Unable to create user!");
		}

		System.out.println("Successfully created user");
	}

	private static void createClient(Keycloak keycloak) {
		String clientId = config.getProperty(KeycloakConst.KEYCLOAK_CLIENT_KEY);

		System.out.println("Creating client");

		ClientRepresentation clientRepresentation = new ClientRepresentation();
		clientRepresentation.setClientId(clientId);
		clientRepresentation.setRedirectUris(Arrays.asList(config.getProperty(KeycloakConst.CALLBACK_URL_WHITELIST_KEY)));
		clientRepresentation.setProtocol(KeycloakConst.OPENID_CONNECT_CLIENT_PROTOCOL);

		Response response = keycloak.realm(config.getProperty(KeycloakConst.KEYCLOAK_REALM_KEY))
				.clients()
				.create(clientRepresentation);
		if(response.getStatus() != HttpStatus.CREATED.value()) {
			throw new IllegalStateException("Unable to create client!");
		}

		System.out.println("Successfully created client");
	}

	private static Properties loadConfig() throws IOException {
		File file = ResourceUtils.getFile("classpath:application.properties");
		InputStream is = Files.newInputStream(file.toPath());
		Properties properties = new Properties();
		properties.load(is);
		return properties;
	}

}
