package com.impoqocx;

/**
 * @author djovanovic
 */
public interface KeycloakConst {

	// CONST
	String MASTER_REALM = "master";
	String OPENID_CONNECT_CLIENT_PROTOCOL = "openid-connect";
	String CALLBACK_URL_WHITELIST_KEY = "impoqocx_keycloak.callback.url.whitelist";

	// KEYCLOAK CONFIG KEYS
	String KEYCLOAK_SERVER_URL_KEY = "keycloak.auth-server-url";
	String KEYCLOAK_REALM_KEY = "keycloak.realm";
	String KEYCLOAK_CLIENT_KEY = "keycloak.resource";

	// USER CONFIG KEYS
	String KEYCLOAK_USER_USERNAME_KEY = "impoqocx_keycloak.username";
	String KEYCLOAK_USER_PASSWORD_KEY = "impoqocx_keycloak.password";
	String KEYCLOAK_USER_EMAIL_KEY = "impoqocx_keycloak.email";

	// SMTP CONFIG KEYS
	String KEYCLOAK_SMTP_HOST_KEY = "impoqocx_keycloak_smtp_host";
	String KEYCLOAK_SMTP_PORT_KEY = "impoqocx_keycloak_smtp_port";
	String KEYCLOAK_SMTP_FROM_KEY = "impoqocx_keycloak_smtp_from";
	String KEYCLOAK_SMTP_FROM_DISPLAY_NAME_KEY = "impoqocx_keycloak_smtp_from_display_name";
	String KEYCLOAK_SMTP_USERNAME_KEY = "impoqocx_keycloak_smtp_username";
	String KEYCLOAK_SMTP_PASSWORD_KEY = "impoqocx_keycloak_smtp_password";

}
