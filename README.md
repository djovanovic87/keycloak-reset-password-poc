This is POC project created to demonstrate reset password flow.

To use it, follow the instructions:
- Run Keycloak as Docker container using:
```
docker pull quay.io/keycloak/keycloak
docker run -p 8080:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin quay.io/keycloak/keycloak:11.0.3
```
- Configure properties in application.properties file.
- Run KeycloakInit class to create a realm, client and user on Keycloak. Reset password flag will be set in the realm. It will also configure SMTP settings mandatory for sending reset password link in email.
- Run this Spring Boot application using:
```
mvn spring-boot:run
```
- Try to access protected resource on http://localhost:8081/protected-resource
- You will be redirected to Keycloak login page where you can either login on reset password